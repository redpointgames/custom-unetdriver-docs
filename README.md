# Custom UNetDriver / UNetConnection implementation guide

This document lists all the "gotchas" that you'll probably run into when you're trying to implement your own custom `UNetDriver` and `UNetConnection` classes.

**IMPORANT: You should read this document in it's entirety before starting your implementation. There is a lot of overlap between sections!**

## Socket state

When you create and assign the ServerConnection field in InitConnect (when a client is connecting to a server), you must set the state to **USOCK_Pending**. If you don’t, the handshake process will silently fail.

When you create a client connection on the server in response to a client connecting to you, you must set the state to **USOCK_Open**. If you don’t, the handshake process will silently fail.

## Driver shutdown

Do not close the socket in `UNetDriver::Shutdown`. Use `UNetDriver::LowLevelDestroy` instead.

If you close the socket in the `UNetDriver::Shutdown` method, then clients will not disconnect properly and the server will instead have to wait for them to timeout.

## Implement all the required methods

You must override the following methods for `UNetDriver`:

- `LowLevelSend`
- `GetSocketSubsystem`
- `IsNetResourceValid` (this should return true if the socket weak pointer is valid, see "Socket and subsystem lifetime" below)
- `IsAvailable` (this should be whether the socket APIs are available at all)
- `InitConnect`
- `InitListen`

You must override the following methods for `UNetConnection`:

- `LowLevelGetRemoteAddress`
- `LowLevelDescribe`
- `LowLevelSend`
- `InitRemoteConnection`
- `InitLocalConnection`

**You will not get a compilation error if you fail to override these. Instead you will get a runtime crash when NewObject tries to instantiate your classes!**

## Override UNetDriver::InitConnectionClass

If your net driver has a specific type of net connection that it must be used with, you will want to override `InitConnectionClass`, like so:

```cpp
bool UMyNetDriver::InitConnectionClass()
{
    // Always use the UMyNetConnection class.
    NetConnectionClass = UMyNetConnection::StaticClass();
    return true;
}
```

Otherwise it will use the connection class defined in config. **(See "Replication flickering" at the end of this document for more information config-related things you need to do!)**

## Setting up outbound connections in InitConnect

When you create and assign the `ServerConnection` field in `UNetDriver::InitConnect`, make sure you set up the server connection correctly:

```cpp
this->ServerConnection = NewObject<UMyNetConnection>();
this->ServerConnection->InitLocalConnection(this, CreatedSocket.Get(), ConnectURL, USOCK_Pending);
this->CreateInitialClientChannels();
```

## Socket and socket subsystem lifetime

If your sockets are dependent on your online subsystem state (for example, if you're implementing peer to peer sockets and you need some kind of handle per online subsystem instance), then you'll want to tie the lifetime of your `ISocketSubsystem` implementation to your online subsystem implementation.

Have your `IOnlineSubsystem` contain a `TSharedPtr<FMySocketSubsystem>` and make the online subsystem manage it's creation and teardown.

Your `ISocketSubsystem` implementation should then be **the only place that holds strong pointers to sockets**. The socket subsystem should have a map of auto-incremented numeric IDs to `TSharedPtr<FMySocket>`. When your socket subsystem creates a socket in `ISocketSubsystem::CreateSocket`, it should add it to the map. When your socket subsystem destroys a socket in `ISocketSubsystem::DestroySocket`, it should `check(Socket.IsUnique())` to make sure the socket subsystem has the only remaining shared reference left. This helps you ensure that you don't have any sockets, net drivers or net connections that are still trying to use the socket.

Your net driver and net connection implementations should use `TWeakPtr<FMySocket>` to store the reference, and they should use `.Pin()` to check if the socket is still available before using it.

## Online subsystem discovery

If your socket subsystem is tied to your online subsystem, you'll need a way of finding the correct `IOnlineSubsystem` instance for the world that the net driver is associated with. You can use the following implementation to find the `UWorld*` for the net driver (and from there you can use `Online::GetSubsystem`):

```cpp
UWorld *UMyNetDriver::FindWorld()
{
    // If we have an explicit world reference set, then return that.
    UWorld *ExplicitWorld = this->GetWorld();
    if (IsValid(ExplicitWorld))
    {
        return ExplicitWorld;
    }

    // When a pending net game is calling InitConnect, we're attached to
    // a pending net game but we don't have a world reference. But we can
    // ask the engine for our world context...
    FWorldContext *WorldContextFromPending = GEngine->GetWorldContextFromPendingNetGameNetDriver(this);
    if (WorldContextFromPending != nullptr)
    {
        return WorldContextFromPending->World();
    }

    // No world could be found.
    return nullptr;
}
```

## Receiving packets

Your `UNetDriver` implementation must implement `TickDispatch` and it needs to poll to receive data from it's associated socket. `UNetDriver` will not do this for you.

You will need to send the received packet to either `ServerConnection` if you're running on a client, or you'll need to iterate through `this->ClientConnections` and find the existing client connection that matches for the address you received the packet on.

## Connectionless handshaking (ugh!)

For some reason, connectionless handshake is not handled at the `UNetDriver` level, despite it being **required** for all net driver implementations. If you don't add the required function calls, or you miss one, things will silently not work, for no apparent reason at all. This is almost certainly the *most painful* part of implementing your own net driver and net connection.

### In InitListen, setup the connectionless handler

You must call `this->InitConnectionlessHandler();` in InitListen, after `InitBase` but (usually) before you create the socket.

### Don't call ReceivedRawPacket directly on the net connection

When you are implementing the packet receive in `UNetDriver`, it's tempting to call `ReceivedRawPacket` on the ServerConnection (client to server) or the ClientConnection (server to client, found in the `ClientConnections` array). **Do not do this!**

Instead, all packets that the `UNetDriver` sends to a `UNetConnection` (regardless of server or client), need to pass through a function that looks like this:

```cpp
void UMyNetConnection::ReceivePacketFromDriver(
    TSharedPtr<FInternetAddr> ReceivedAddr,
    uint8 *Buffer,
    int32 BufferSize)
{
    if (this->bInConnectionlessHandshake)
    {
        const ProcessedPacket RawPacket =
            this->Driver->ConnectionlessHandler->IncomingConnectionless(ReceivedAddr, Buffer, BufferSize);
        TSharedPtr<StatelessConnectHandlerComponent> StatelessConnect = this->Driver->StatelessConnectComponent.Pin();
        bool bRestartedHandshake = false;
        if (!RawPacket.bError && StatelessConnect->HasPassedChallenge(ReceivedAddr, bRestartedHandshake) &&
            !bRestartedHandshake)
        {
            if (this->StatelessConnectComponent.IsValid())
            {
                int32 ServerSequence = 0;
                int32 ClientSequence = 0;
                StatelessConnect->GetChallengeSequence(ServerSequence, ClientSequence);
                this->InitSequence(ClientSequence, ServerSequence);
            }

            if (this->Handler.IsValid())
            {
                this->Handler->BeginHandshaking();
            }

            this->bInConnectionlessHandshake = false;
            
            // Reset the challenge data for the future
            if (StatelessConnect.IsValid())
            {
                StatelessConnect->ResetChallengeData();
            }

            int32 RawPacketSize = FMath::DivideAndRoundUp(RawPacket.CountBits, 8);
            if (RawPacketSize == 0)
            {
                // No actual data to receive.
                return;
            }

            // Forward the data from the processed packet.
            this->ReceivedRawPacket(RawPacket.Data, RawPacketSize);
            return;
        }
    }

    // Forward data onto connection.
    this->ReceivedRawPacket(Buffer, BufferSize);
}
```

Yes, exactly like that. No, I don't know why the base `UNetConnection` class doesn't handle this for you.

You will need to declare `bInConnectionlessHandshake` as a private bool in your `UNetConnection` class. It should be `false` by default.

### Set bInConnectionlessHandshake when accepting new connections.

See below for connection acceptance logic.

## Accepting new connections

When you are running on the server and you get a new connection from the client, you need to set up the client connection correctly. Your code should look like this:

```cpp
// We are a server and we have a new client connection, construct
// the connection object and add it to our list of client connections.
//
// Server connections to clients *MUST* be created in the USOCK_Open status,
// otherwise handshaking (challenge response) won't be handled correctly
// on the server.
UMyNetConnection *ClientConnection = NewObject<UMyNetConnection>();
check(ClientConnection);
ClientConnection->InitRemoteConnection(
    this,
    &AcceptedSocket.Get(),
    World ? World->URL : FURL(),
    *IncomingAddr,
    USOCK_Open);
Notify->NotifyAcceptedConnection(ClientConnection);
this->AddClientConnection(ClientConnection);
if (this->ConnectionlessHandler.IsValid() && this->StatelessConnectComponent.IsValid())
{
    ClientConnection->bInConnectionlessHandshake = true;
}
```

Again, pay attention to the **USOCK_Open** state and setting **bInConnectionlessHandshake** to true where necessary. If either of these things are wrong, clients will silently fail to connect.

## Sending packets from the UNetDriver

You must implement both `UNetDriver::LowLevelSend` and `UNetConnection::LowLevelSend`. Their implementations **are not the same** and you **can not delegate UNetConnection::LowLevelSend to UNetDriver::LowLevelSend!** They are slightly different!

For the `UNetDriver::LowLevelSend` implementation, you will want something like this:

```cpp
void UMyNetDriver::LowLevelSend(
    TSharedPtr<const FInternetAddr> Address,
    void *Data,
    int32 CountBits,
    FOutPacketTraits &Traits)
{
    TSharedPtr<FMySocket> SocketPinned = this->Socket.Pin();
    if (SocketPinned.IsValid() && Address.IsValid() && Address->IsValid())
    {
        const uint8 *SendData = reinterpret_cast<const uint8 *>(Data);
        if (this->ConnectionlessHandler.IsValid())
        {
            const ProcessedPacket ProcessedData =
                this->ConnectionlessHandler->OutgoingConnectionless(Address, (uint8 *)SendData, CountBits, Traits);

            if (!ProcessedData.bError)
            {
                SendData = ProcessedData.Data;
                CountBits = ProcessedData.CountBits;
            }
            else
            {
                CountBits = 0;
            }
        }

        if (CountBits > 0)
        {
            int32 BytesToSend = FMath::DivideAndRoundUp(CountBits, 8);
            int32 SentBytes = 0;

            // Our sendto will find the correct socket to send over.
            if (!SocketPinned->SendTo(SendData, BytesToSend, SentBytes, *Address))
            {
                UE_LOG(
                    LogNet,
                    Warning,
                    TEXT("UMyNetDriver::LowLevelSend: Could not send %d data over socket to %s!"),
                    BytesToSend,
                    *Address->ToString(true));
            }
        }
    }
    else
    {
        UE_LOG(
            LogNet,
            Error,
            TEXT("UMyNetDriver::LowLevelSend: Could not send data because either the socket or address is "
                 "invalid!"));
    }
}
```

For the `UNetConnection::LowLevelSend` implementation, you will want something like this:

```cpp
void UMyNetConnection::LowLevelSend(void *Data, int32 CountBits, FOutPacketTraits &Traits)
{
    TSharedPtr<FMySocket> SocketPinned = this->Socket.Pin();
    if (SocketPinned.IsValid())
    {
        const uint8 *SendData = reinterpret_cast<const uint8 *>(Data);

        // Process any packet modifiers
        if (this->Handler.IsValid() && !this->Handler->GetRawSend())
        {
            const ProcessedPacket ProcessedData =
                this->Handler->Outgoing(reinterpret_cast<uint8 *>(Data), CountBits, Traits);

            if (!ProcessedData.bError)
            {
                SendData = ProcessedData.Data;
                CountBits = ProcessedData.CountBits;
            }
            else
            {
                CountBits = 0;
            }
        }

        int32 BytesSent = 0;
        int32 BytesToSend = FMath::DivideAndRoundUp(CountBits, 8);

        if (BytesToSend > 0)
        {
            if (!SocketPinned->SendTo(SendData, BytesToSend, BytesSent, *RemoteAddr))
            {
                UE_LOG(
                    LogNet,
                    Warning,
                    TEXT("UMyNetConnection::LowLevelSend: Could not send %d data over socket to %s!"),
                    BytesToSend,
                    *RemoteAddr->ToString(true));
            }
        }
    }
    else
    {
        UE_LOG(LogNet, Error, TEXT("UMyNetConnection::LowLevelSend: Could not send data because the socket is gone!"));
    }
}
```

Again, note the amount of unnecessary code that packet handlers and handshaking introduces, when all that stuff could have just been handled inside the base `UNetDriver` and `UNetConnection` classes to begin with. *Sigh...*

## Replication flickering

If you are experiencing "flickering" on clients, where the player controller and pawn seem to be constantly recreated, then the RelevancyTimeout config is 0. If you’re inheriting from UNetDriver, you need to copy the configs the engine has already set up for UIpNetDriver and set them for your custom version.

You want to add configuration like this:

```ini
[/Script/OnlineSubsystemMy.MyNetDriver]
!ChannelDefinitions=ClearArray
+ChannelDefinitions=(ChannelName=Control, ClassName=/Script/Engine.ControlChannel, StaticChannelIndex=0, bTickOnCreate=true, bServerOpen=false, bClientOpen=true, bInitialServer=false, bInitialClient=true)
+ChannelDefinitions=(ChannelName=Voice, ClassName=/Script/Engine.VoiceChannel, StaticChannelIndex=1, bTickOnCreate=true, bServerOpen=true, bClientOpen=true, bInitialServer=true, bInitialClient=true)
+ChannelDefinitions=(ChannelName=Actor, ClassName=/Script/Engine.ActorChannel, StaticChannelIndex=-1, bTickOnCreate=false, bServerOpen=true, bClientOpen=false, bInitialServer=false, bInitialClient=false)
NetConnectionClassName="/Script/OnlineSubsystemMy.MyNetConnection"
ConnectionTimeout=80.0
InitialConnectTimeout=120.0
NetServerMaxTickRate=30
MaxNetTickRate=120
KeepAliveTime=0.2
MaxClientRate=15000
MaxInternetClientRate=10000
RelevantTimeout=5.0
SpawnPrioritySeconds=1.0
ServerTravelPause=4.0
TimeoutMultiplierForUnoptimizedBuilds=1
RecentlyDisconnectedTrackingTime=120
```

### If your driver is defined inside your own project

Use `UCLASS(transient, config=Engine)` on the net driver and net connection class declarations. Put the configuration values in `DefaultEngine.ini`, and omit the `ChannelDefinitions` entries.

### If your driver is defined inside a plugin

Use `UCLASS(transient, config=PluginName)` on the net driver and net connection class declarations. Put the configuration values in `Plugins/PluginName/Config/BasePluginName.ini` **and** `Plugins/PluginName/Config/DefaultPluginName.ini`, and keep the `ChannelDefinitions` entries. You *must* have both files, as Unreal Engine looks at different files depending on whether the plugin is a project plugin or installed as an engine plugin (via the marketplace).
